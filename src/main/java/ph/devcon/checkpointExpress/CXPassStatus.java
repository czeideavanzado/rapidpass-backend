/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package ph.devcon.checkpointExpress;
@org.apache.avro.specific.AvroGenerated
public enum CXPassStatus implements org.apache.avro.generic.GenericEnumSymbol<CXPassStatus> {
  ACTIVE, INACTIVE  ;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"enum\",\"name\":\"CXPassStatus\",\"namespace\":\"ph.devcon.checkpointExpress\",\"symbols\":[\"ACTIVE\",\"INACTIVE\"]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }
  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
}
