/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package ph.devcon.rapidpass.model;
@org.apache.avro.specific.AvroGenerated
public enum PassType implements org.apache.avro.generic.GenericEnumSymbol<PassType> {
  INDIVIDUAL, VEHICLE  ;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"enum\",\"name\":\"PassType\",\"namespace\":\"ph.devcon.rapidpass.model\",\"symbols\":[\"INDIVIDUAL\",\"VEHICLE\"]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }
  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
}
