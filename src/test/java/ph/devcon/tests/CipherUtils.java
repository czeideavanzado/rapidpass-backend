package ph.devcon.tests;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class CipherUtils {

    public static final String PASS_PHRASE = "supersecretpassphrase";
    public static final String KEY_ALGORITHM = "AES";
    public static final String CIPHER_ALGORITHM = "AES/ECB/PKCS5Padding";
    public static final int KEY_LENGTH = 16;
//    public static final String KEY_ALGORITHM = "DES";
//    public static final String CIPHER_ALGORITHM = "DES";
//    public static final int KEY_LENGTH = 8;
    Logger logger = LoggerFactory.getLogger(CipherUtils.class.getCanonicalName());

    private SecretKeySpec makeSecretKey(String passPhrase, int length) throws UnsupportedEncodingException {
        if (passPhrase.length() < length) {
            int missingLength = length - passPhrase.length();
            for (int i = 0; i < missingLength; i++) {
                passPhrase += " ";
            }
        }
        byte[] key = passPhrase.substring(0, length).getBytes("UTF-8");
//        logger.info("Key Length: {}", key.length);
        SecretKeySpec secretKey = new SecretKeySpec(key, KEY_ALGORITHM);
        return secretKey;
    }

    public byte[] encryptData(byte[] plaintextBytes) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        SecretKeySpec secretKey = makeSecretKey(PASS_PHRASE, KEY_LENGTH);
//        byte[] plaintextBytes = data.getBytes("UTF8");

        Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);

        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        byte[] encryptedBytes = cipher.doFinal(plaintextBytes);
        return encryptedBytes;
    }

    public byte[] decryptData(byte[] encryptedDataBytes) throws UnsupportedEncodingException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        SecretKeySpec secretKey = makeSecretKey(PASS_PHRASE, KEY_LENGTH);
//        byte[] encryptedDataBytes = encryptedData.getBytes("UTF8");

        Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);

        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        byte[] decryptedBytes = cipher.doFinal(encryptedDataBytes);
        return decryptedBytes;
    }

    public byte[] hashData(String data) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(data.getBytes("UTF-8"));
        md.update("supersecretpassphrase".getBytes());
        byte[] digest = md.digest();
        return digest;
    }
}
