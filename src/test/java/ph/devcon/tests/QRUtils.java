package ph.devcon.tests;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.CharacterSetECI;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.google.zxing.qrcode.detector.FinderPattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

public class QRUtils {

    Logger logger = LoggerFactory.getLogger(QRUtils.class.getCanonicalName());

    public void createQRImage(String filename, byte[] qrCodeBytes, int size, String fileType, int qrVersion)
            throws WriterException, IOException {

        // QR Code text has to be base64 encoded, else this will not work
        String qrCodeText = Base64.getEncoder().encodeToString(qrCodeBytes);

        File qrFile = new File(filename);

        // Create the ByteMatrix for the QR-Code that encodes the given String
//        Hashtable<EncodeHintType, ErrorCorrectionLevel> hintMap = new Hashtable<EncodeHintType, ErrorCorrectionLevel>();
        Map hintMap = new HashMap();
        hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
        if (qrVersion > 0) {
            hintMap.put(EncodeHintType.QR_VERSION, qrVersion);
        }

        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix byteMatrix = qrCodeWriter.encode(qrCodeText, BarcodeFormat.QR_CODE, size, size, hintMap);

        // Make the BufferedImage that will hold the QRCode
        int matrixWidth = byteMatrix.getWidth();
        BufferedImage image = new BufferedImage(matrixWidth, matrixWidth, BufferedImage.TYPE_INT_RGB);
        image.createGraphics();

        Graphics2D graphics = (Graphics2D) image.getGraphics();
        graphics.setColor(Color.WHITE);
        graphics.fillRect(0, 0, matrixWidth, matrixWidth);
        // Paint and save the image using the ByteMatrix
        graphics.setColor(Color.BLACK);

        for (int i = 0; i < matrixWidth; i++) {
            for (int j = 0; j < matrixWidth; j++) {
                if (byteMatrix.get(i, j)) {
                    graphics.fillRect(i, j, 1, 1);
                }
            }
        }
        ImageIO.write(image, fileType, qrFile);
    }

    public byte[] readQRImage(String filename) throws IOException, NotFoundException {

        File qrCodeFile = new File(filename);
        BufferedImage bufferedImage = ImageIO.read(qrCodeFile);
        LuminanceSource source = new BufferedImageLuminanceSource(bufferedImage);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

        Result result = new MultiFormatReader().decode(bitmap);

        ResultPoint[] resultPoints = result.getResultPoints();
        for (int i = 0; i < resultPoints.length; i++) {
            ResultPoint resultPoint = resultPoints[i];
            System.out.print("  [" + i + "]:");
            System.out.print(" x = " + resultPoint.getX());
            System.out.print(", y = " + resultPoint.getY());
            if (resultPoint instanceof FinderPattern)
                System.out.print(", estimatedModuleSize = "
                        + ((FinderPattern) resultPoint).getEstimatedModuleSize());
            System.out.println();
        }

        String encodedString = result.getText();
        return Base64.getDecoder().decode(encodedString);
    }
}
