package ph.devcon.tests;

import com.google.zxing.NotFoundException;
import com.google.zxing.WriterException;
import org.apache.avro.io.*;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;
import ph.devcon.checkpointExpress.*;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Date;

public class TestCXPass {

    public static final int QR_IMAGE_SIZE = 4098;
    public static final String QR_FILE_TYPE = "png";
    private static long VALID_START_DATE = new Date().getTime();
    private CheckpointExpressPass cxPass;
    private QRUtils qrUtils = new QRUtils();
    private CipherUtils cipherUtils = new CipherUtils();

    Logger logger = LoggerFactory.getLogger(TestCXPass.class.getCanonicalName());

    @BeforeSuite
    public void createQRImages() throws IOException, WriterException, NoSuchAlgorithmException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchPaddingException {
        String concrolCode = "12345678";
        logger.info("CXPass Payload, length: {}, data: {}", concrolCode.length(), concrolCode.toString());
        logger.info("Hashed Pass length: {}", Base64.getEncoder().encode(concrolCode.getBytes("ISO-8859-1")).length);
        qrUtils.createQRImage("CxPlainPass.png", concrolCode.getBytes("ISO-8859-1"), QR_IMAGE_SIZE, QR_FILE_TYPE,1);

        cxPass = createPass();
        logger.info("CXPass Payload, length: {}, data: {}", cxPass.toString().length(), cxPass.toString());
        byte[] hashedPass = cipherUtils.hashData(cxPass.getApprovalCode().toString());
        Assert.assertNotNull(cxPass);
        logger.info("Hashed Pass length: {}", Base64.getEncoder().encode(hashedPass).length);
        qrUtils.createQRImage("CxHashedPass.png", hashedPass, QR_IMAGE_SIZE, QR_FILE_TYPE,2);

//        byte[] encryptedJsonPass = cipherUtils.encryptData(cxPass.toString().getBytes("UTF-8"));
//        logger.info("Encrypted Json Pass length: {}", Base64.getEncoder().encode(encryptedJsonPass).length);
//        qrUtils.createQRImage("CxEncryptedJsonPass.png", encryptedJsonPass, QR_IMAGE_SIZE, QR_FILE_TYPE, 10);

        byte[] serializedPass = serializeCxPass(cxPass);
        logger.info("Serialized Pass length: {}", Base64.getEncoder().encode(serializedPass).length);
        qrUtils.createQRImage("CxSerializedPass.png", serializedPass, QR_IMAGE_SIZE, QR_FILE_TYPE, 3);

        BinaryCXPass binaryCXPass = createBinaryPass();
        logger.info("Binary CXPass Payload, length: {}, data: {}", binaryCXPass.toString().length(), binaryCXPass.toString());
        byte[] serializedBinaryPass = serializeBinaryPass(binaryCXPass);
        logger.info("Serialized Binary Pass length: {}", serializedBinaryPass.length);
        logger.info("Base64 Binary Pass length: {}", Base64.getEncoder().encode(serializedBinaryPass).length);
        qrUtils.createQRImage("CxSerializedBinaryPass.png", serializedBinaryPass, QR_IMAGE_SIZE, QR_FILE_TYPE, 2);

//        byte[] encryptedSerializedData = cipherUtils.encryptData(serializedPass);
//        logger.info("Encrypted Serialized Pass length: {}", Base64.getEncoder().encode(encryptedSerializedData).length);
//        qrUtils.createQRImage("CxEncSerializedPass.png", encryptedSerializedData, QR_IMAGE_SIZE, QR_FILE_TYPE, 4);

        byte[] approvalCodeBytes = Base64.getEncoder().encode("1234567890".getBytes("UTF-8"));
        logger.info("Approval Code length: {}", Base64.getEncoder().encode(approvalCodeBytes).length);
        qrUtils.createQRImage("CxApprovalCodeOnlyPass.png", approvalCodeBytes, QR_IMAGE_SIZE, QR_FILE_TYPE, 2);

        byte[] encryptedBinaryData = cipherUtils.encryptData(serializedBinaryPass);
        byte[] mergedData = new byte[8 + serializedBinaryPass.length];
        System.arraycopy(encryptedBinaryData, 0, mergedData, 0, 8);
        System.arraycopy(serializedBinaryPass, 0, mergedData, 8, serializedBinaryPass.length);
        logger.info("Signed Pass length: {}", Base64.getEncoder().encode(mergedData).length);
        qrUtils.createQRImage("CxSignedBinaryPass.png", mergedData, QR_IMAGE_SIZE, QR_FILE_TYPE, 3);
    }

    @Test(priority = 1)
    @Ignore
    public void testEncryptedJsonPass() throws IOException, NotFoundException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        System.out.println("Reading Encrypted JSON Pass");
        byte[] encryptedData = qrUtils.readQRImage("CxEncryptedJsonPass.png");
//        logger.info("Encrypted QR Data, length: {}, bytes: {}", encryptedData.length, encryptedData);
        Assert.assertNotNull(encryptedData);
        byte[] decyptedData = cipherUtils.decryptData(encryptedData);
        Assert.assertNotNull(decyptedData);
        logger.info("Decrypted JSON Data: {}", new String(decyptedData));
    }

    @Test(priority = 2)
    @Ignore
    public void testEncryptedSerializedPass() throws IOException, NotFoundException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        System.out.println("Reading Encrypted CX Serialized Pass");
        byte[] encryptedSerializedData = qrUtils.readQRImage("CxEncSerializedPass.png");
        Assert.assertNotNull(encryptedSerializedData);
        byte[] decryptedData = cipherUtils.decryptData(encryptedSerializedData);
        Assert.assertNotNull(decryptedData);
        CheckpointExpressPass cxPassFromQR = deserializeCxPass(decryptedData);
        Assert.assertNotNull(cxPassFromQR);
        Assert.assertEquals(cxPassFromQR.getEffectiveDate(), cxPass.getEffectiveDate());
        Assert.assertEquals(cxPassFromQR.getExpirationDate(), cxPass.getExpirationDate());
        Assert.assertEquals(cxPassFromQR.getType(), cxPass.getType());
        Assert.assertEquals(cxPassFromQR.getPlateNumberOrID().toString(), cxPass.getPlateNumberOrID().toString());
//        Assert.assertEquals(cxPassFromQR.getBadgeID(), cxPass.getBadgeID());
        Assert.assertEquals(cxPassFromQR.getApprovalCode().toString(), cxPass.getApprovalCode().toString());
        Assert.assertEquals(cxPassFromQR.getEffectiveDate(), cxPass.getEffectiveDate());
    }

    @Test(priority = 3)
    @Ignore
    public void testSerializedData() throws IOException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, NotFoundException {
        System.out.println("Reading CX Serialized Pass");
        byte[] serializedData = qrUtils.readQRImage("CxSerializedPass.png");
//        Assert.assertNotNull(encryptedSerializedData);
//        byte[] serializedData = cipherUtils.decryptData(encryptedSerializedData);
        Assert.assertNotNull(serializedData);
        CheckpointExpressPass cxPassFromQR = deserializeCxPass(serializedData);
        Assert.assertNotNull(cxPassFromQR);
        Assert.assertEquals(cxPassFromQR.getEffectiveDate(), cxPass.getEffectiveDate());
        Assert.assertEquals(cxPassFromQR.getExpirationDate(), cxPass.getExpirationDate());
        Assert.assertEquals(cxPassFromQR.getType(), cxPass.getType());
        Assert.assertEquals(cxPassFromQR.getPlateNumberOrID().toString(), cxPass.getPlateNumberOrID().toString());
//        Assert.assertEquals(cxPassFromQR.getBadgeID(), cxPass.getBadgeID());
        Assert.assertEquals(cxPassFromQR.getApprovalCode().toString(), cxPass.getApprovalCode().toString());
        Assert.assertEquals(cxPassFromQR.getEffectiveDate(), cxPass.getEffectiveDate());
    }

    @Test(priority = 4)
    @Ignore
    public void testSerializedBinaryData() throws IOException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, NotFoundException {
        System.out.println("Reading Binary CX Serialized Pass");
        byte[] serializedData = qrUtils.readQRImage("CxSerializedBinaryPass.png");
        Assert.assertNotNull(serializedData);
//        CheckpointExpressPass cxPassFromQR = deserializeCxPass(serializedData);
//        Assert.assertNotNull(cxPassFromQR);
//        Assert.assertEquals(cxPassFromQR.getEffectiveDate(), cxPass.getEffectiveDate());
//        Assert.assertEquals(cxPassFromQR.getExpirationDate(), cxPass.getExpirationDate());
//        Assert.assertEquals(cxPassFromQR.getType(), cxPass.getType());
//        Assert.assertEquals(cxPassFromQR.getPlateNumberOrID().toString(), cxPass.getPlateNumberOrID().toString());
//        Assert.assertEquals(cxPassFromQR.getApprovalCode().toString(), cxPass.getApprovalCode().toString());
//        Assert.assertEquals(cxPassFromQR.getEffectiveDate(), cxPass.getEffectiveDate());
    }

    @Test(priority = 5)
    @Ignore
    public void testApprovalOnlyData() throws IOException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, NotFoundException {
        System.out.println("Reading Approval Code Only Pass");
        byte[] serializedData = qrUtils.readQRImage("CxApprovalCodeOnlyPass.png");
        Assert.assertNotNull(serializedData);
//        CheckpointExpressPass cxPassFromQR = deserializeCxPass(serializedData);
//        Assert.assertNotNull(cxPassFromQR);
//        Assert.assertEquals(cxPassFromQR.getEffectiveDate(), cxPass.getEffectiveDate());
//        Assert.assertEquals(cxPassFromQR.getExpirationDate(), cxPass.getExpirationDate());
//        Assert.assertEquals(cxPassFromQR.getType(), cxPass.getType());
//        Assert.assertEquals(cxPassFromQR.getPlateNumberOrID().toString(), cxPass.getPlateNumberOrID().toString());
//        Assert.assertEquals(cxPassFromQR.getApprovalCode().toString(), cxPass.getApprovalCode().toString());
//        Assert.assertEquals(cxPassFromQR.getEffectiveDate(), cxPass.getEffectiveDate());
    }

    private CheckpointExpressPass createPass() throws UnsupportedEncodingException {
        cxPass = CheckpointExpressPass.newBuilder()
                .setApprovalCode("1234567890")
                .setType(CXPassType.VEHICLE)
                .setPlateNumberOrID("ABC4321")
                .setCategory(CXPassCategory.FREIGHT)
                .setEffectiveDate(VALID_START_DATE)
                .setExpirationDate(new Date().getTime())
                .build();
        return cxPass;
    }

    private BinaryCXPass createBinaryPass() throws UnsupportedEncodingException {
        BinaryCXPass cxPass = new BinaryCXPass();
//        cxPass.setApprovalId(UUID.randomUUID().toString());
        cxPass.setApprovalCode(1234567890);
        cxPass.setType(CXPassType.VEHICLE);
        cxPass.setPlateNumberOrID("ABC4321");
        cxPass.setCategory(CXPassCategory.FREIGHT);
//        cxPass.setValidStarting(VALID_START_DATE);
        cxPass.setValidUntil(new Date().getTime());
        return cxPass;
    }

    private byte[] serializeBinaryPass(BinaryCXPass cxPass) throws IOException {
        DatumWriter<BinaryCXPass> passWriter = new SpecificDatumWriter<>(BinaryCXPass.getClassSchema());
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        BinaryEncoder encoder = EncoderFactory.get().binaryEncoder(outputStream, null);
        passWriter.write(cxPass, encoder);
        encoder.flush();
        outputStream.flush();
        String serializedData = Base64.getEncoder().encodeToString(outputStream.toByteArray());
        logger.info("Serialized data: " + serializedData);
        return outputStream.toByteArray();
    }

    private byte[] serializeCxPass(CheckpointExpressPass cxPass) throws IOException {
        DatumWriter<CheckpointExpressPass> passWriter = new SpecificDatumWriter<>(CheckpointExpressPass.getClassSchema());
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        BinaryEncoder encoder = EncoderFactory.get().binaryEncoder(outputStream, null);
        passWriter.write(cxPass, encoder);
        encoder.flush();
        outputStream.flush();
        String serializedData = Base64.getEncoder().encodeToString(outputStream.toByteArray());
        logger.info("Serialized data: " + serializedData);
        return outputStream.toByteArray();
    }

    private CheckpointExpressPass deserializeCxPass(byte[] serializedData) throws IOException {
        BinaryDecoder decoder = DecoderFactory.get().binaryDecoder(serializedData, null);
        DatumReader<CheckpointExpressPass> passReader = new SpecificDatumReader< >(CheckpointExpressPass.getClassSchema());
        return passReader.read(null, decoder);
    }

}
